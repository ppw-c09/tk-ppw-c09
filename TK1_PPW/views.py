from django.shortcuts import render
from RumahSakit.models import RS

# Create your views here.
def home(request):
    return render(request, "base.html")