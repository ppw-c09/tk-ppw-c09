from django.db import models
from django.core.validators import MinValueValidator 

# Create your models here.
class RS(models.Model):
    nama = models.TextField(max_length=100, unique=True)
    alamat = models.TextField(max_length=150, unique=True)
    jml_prwt = models.PositiveIntegerField()
    jml_non = models.PositiveIntegerField()


    def __str__(self):
        return self.nama