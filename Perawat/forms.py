from django.forms import ModelForm, TextInput, NumberInput, EmailInput
from django import forms
from Perawat.models import Perawat

class FormPerawat(ModelForm):
    class Meta:
        PROVINSI = (
        ('1', 'Aceh'),
        ('2', 'Sumatra Utara'),
        ('3', 'Sumatra Barat'),
        ('4', 'Riau'),
        ('5', 'Kepulauan Riau'),
        ('6', 'Bengkulu'),
        ('7', 'Jambi'),
        ('8', 'Sumatra Selatan'),
        ('9', 'Bangka Belitung'),
        ('10', 'Lampung'),
        ('11', 'Banten'),
        ('12', 'Jawa Barat'),
        ('13', 'DKI Jakarta'),
        ('14', 'Jawa Tengah'),
        ('15', 'Yogyakarta'),
        ('16', 'Jawa Timur'),
        ('17', 'Bali'),
        ('18', 'NTB'),
        ('19', 'NTT'),
        ('20', 'Kalimantan Timur'),
        ('21', 'Kalimantan Barat'),
        ('22', 'Kalimantan Selatan'),
        ('23', 'Kalimantan Tengah'),
        ('24', 'Kalimantan Utara'),
        ('25', 'Gorontalo'),
        ('26', 'Sulawesi Barat'),
        ('27', 'Sulawesi Tengah'),
        ('28', 'Sulawesi Selatan'),
        ('29', 'Sulawesi Tenggara'),
        ('30', 'Sulawesi Utara'),
        ('31', 'Maluku'),
        ('32', 'Maluku Utara'),
        ('33', 'Papua Barat'),
        ('34', 'Papua'),
    )
        model = Perawat
        fields = [
            'nama',
            'umur',
            'email',
            'provinsi',
            'kota',
            'no_telp',
            'pengalaman',
            'alasan'
        ]
        labels = {
            'pengalaman' : 'Ceritakan pengalaman Anda dalam bidang keperawatan:',
            'alasan' : 'Alasan ingin menjadi relawan:'
        }
        widgets = {
            'nama' : TextInput(attrs={'class':'form-control form-control-sm', 'required': True}),
            'umur' : NumberInput(attrs={'class':'form-control form-control-sm', 'required': True}),
            'email' : EmailInput(attrs={'class':'form-control form-control-sm', 'required': True}),
            'provinsi' : forms.Select(choices=PROVINSI, attrs={'class':'form-control form-control-sm', 'required': True}),
            'kota' : TextInput(attrs={'class':'form-control form-control-sm', 'required': True}),
            'no_telp' : NumberInput(attrs={'class':'form-control form-control-sm', 'required': True}),
            'pengalaman' : forms.Textarea(attrs={'class':'form-control form-control-sm', 'required': True}),
            'alasan' : forms.Textarea(attrs={'class':'form-control form-control-sm', 'required': True}),
        }
        