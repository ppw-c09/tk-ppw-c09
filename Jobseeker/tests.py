from django.test import TestCase
from django.urls import resolve, reverse
from .models import Jobseeker
from .forms import FormPelamar
from Kantor.models import Kantor
from .views import testFrontEnd
from django.test import Client
from .apps import JobseekerConfig
from django.apps import apps
# Create your tests here.


class ModelTestJobseeker(TestCase):
    def setUp(self):
        self.pelamar = Jobseeker.objects.create(
            nama_pelamar="Joni",
            umur_pelamar=17,
            provinsi_pelamar="1",
            kota_pelamar="Bogor",
            pendidikan_terakhir="MA",
        )

    def test_instance_created(self):
        self.assertEqual(Jobseeker.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.pelamar), "Joni")


class FormTest(TestCase):
    def test_form_is_valid(self):
        form_jobseeker = FormPelamar(
            data={
                'nama_pelamar': "Joni",
                'umur_pelamar': 17,
                'provinsi_pelamar': "1",
                'kota_pelamar': "Bogor",
                'pendidikan_terakhir': "MA",
            }
        )
        self.assertTrue(form_jobseeker.is_valid())

    def test_form_invalid(self):
        form_jobseeker = FormPelamar(
            data={
            }
        )
        self.assertFalse(form_jobseeker.is_valid())


class UrlTest(TestCase):

    def test(self):
        kantor = Kantor.objects.create(
            nama_pekerjaan="PM",
            alamat="jalancinta",
            tipe_pekerjaan="Fulltime",
        )
        kantor.save()
        found = resolve(reverse('daftar_kerja', args=[1]))
        self.assertTrue(found.func == testFrontEnd)


class ViewTest(TestCase):
    def setUp(self):
        kantor = Kantor.objects.create(
            nama_pekerjaan="PM",
            alamat="jalancinta",
            tipe_pekerjaan="Fulltime",
        )
        kantor.save()

    def test_regist_POST(self):
        response = Client().post(reverse('daftar_kerja', args=[1]),
                                 data={
                                     'nama_pelamar': 'bangjago',
                                     'umur_pelamar': 17,
                                     'provinsi_pelamar': '1',
                                     'kota_pelamar': 'Bandung',
                                     'pendidikan_terakhir': 'MA'
        })
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = Client().get(reverse('daftar_kerja', args=[1]))
        self.assertTemplateUsed(response, 'Jobseeker/addPelamar.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post(reverse('daftar_kerja', args=[1]),
                                 data={'nama_pelamar': ' '})
        self.assertTemplateUsed(response, 'Jobseeker/addPelamar.html')
        self.assertEqual(response.status_code, 200)


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(JobseekerConfig.name, 'Jobseeker')
        self.assertEqual(apps.get_app_config('Jobseeker').name, 'Jobseeker')
