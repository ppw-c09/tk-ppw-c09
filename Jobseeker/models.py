from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from Kantor.models import Kantor
# Create your models here.


class Jobseeker(models.Model):
    LAST_ED = (
        ('MA', 'SMA'),
        ('S1', 'Sarjana'),
        ('S2', 'Pascasarjana'),
    )

    PROVINSI = (
        ('1', 'Aceh'),
        ('2', 'Sumatra Utara'),
        ('3', 'Sumatra Barat'),
        ('4', 'Riau'),
        ('5', 'Kepulauan Riau'),
        ('6', 'Bengkulu'),
        ('7', 'Jambi'),
        ('8', 'Sumatra Selatan'),
        ('9', 'Bangka Belitung'),
        ('10', 'Lampung'),
        ('11', 'Banten'),
        ('12', 'Jawa Barat'),
        ('13', 'Jakarta'),
        ('14', 'Jawa Tengah'),
        ('15', 'Yogyakarta'),
        ('16', 'Jawa Timur'),
        ('17', 'Bali'),
        ('18', 'NTB'),
        ('19', 'NTT'),
        ('20', 'Kalimantan Timur'),
        ('21', 'Kalimantan Barat'),
        ('22', 'Kalimantan Selatan'),
        ('23', 'Kalimantan Tengah'),
        ('24', 'Kalimantan Utara'),
        ('25', 'Gorontalo'),
        ('26', 'Sulawesi Barat'),
        ('27', 'Sulawesi Tengah'),
        ('28', 'Sulawesi Selatan'),
        ('29', 'Sulawesi Tenggara'),
        ('30', 'Sulawesi Utara'),
        ('31', 'Maluku'),
        ('32', 'Maluku Utara'),
        ('33', 'Papua Barat'),
        ('34', 'Papua'),
    )
    nama_pelamar = models.CharField(max_length=64)
    umur_pelamar = models.IntegerField(
        validators=[
            MaxValueValidator(55),
            MinValueValidator(17)
        ])
    pekerjaan_dilamar = models.ForeignKey(Kantor,
                                          on_delete=models.CASCADE, null=True, blank=True)
    provinsi_pelamar = models.CharField(max_length=2, choices=PROVINSI)
    kota_pelamar = models.CharField(max_length=15)
    pendidikan_terakhir = models.CharField(max_length=2, choices=LAST_ED)

    def __str__(self):
        return self.nama_pelamar
