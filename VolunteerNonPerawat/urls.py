from django.urls import path

from . import views

app_name = 'VolunteerNonPerawat'

urlpatterns = [
    path('<int:id>', views.index, name='index'),
    path('add/<int:id>', views.add_vnp, name='add-vnp'),
    # path('delete/<int:id>/', views.delete_vnp, name='delete-vnp')
]