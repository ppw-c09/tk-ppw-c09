from django.test import TestCase, Client
from RumahSakit.models import RS
from RumahSakit.forms import RSForm
from django.urls import reverse
from RumahSakit.apps import RumahsakitConfig
from django.apps import apps
from Perawat.models import Perawat
from VolunteerNonPerawat.models import VolunteerNonPerawat

# Create your tests here.
class ModelTest(TestCase):
    def setUp(self):
        self.RS_test = RS.objects.create(
            nama = "RSJ Test",
            alamat = "Grogol",
            jml_prwt = 1,
            jml_non = 1,
        )

    def test_instance_created(self):
        self.assertEqual(RS.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.RS_test), "RSJ Test")
    
    def test_nama(self):
        self.assertEqual(self.RS_test.nama, "RSJ Test")

    def test_alamat(self):
        self.assertEqual(self.RS_test.alamat, "Grogol")
    
    def test_jmlprwt(self):
        self.assertEqual(self.RS_test.jml_prwt, 1)

    def test_jmlnon(self):
        self.assertEqual(self.RS_test.jml_non, 1)


class FormTest(TestCase):
    def test_form_is_valid(self):
        form_RS = RSForm(
            data={
                'nama': "RSJ Test",
                'alamat': "Grogol",
                'jml_prwt': 1,
                "jml_non": 1,
            }
        )
        self.assertTrue(form_RS.is_valid())

    def test_form_invalid(self):
        form_RS = RSForm(
            data={
            }
        )
        self.assertFalse(form_RS.is_valid())


class UrlTest(TestCase):

    def test_url(self):
        RS_test = RS.objects.create(
            nama = "RSJ Test",
            alamat = "Grogol",
            jml_prwt = 1,
            jml_non = 1,
        )
        RS_test.save()
        

class ViewTest(TestCase):

    def test_daftar_GET(self):
        response = self.client.get(reverse("RumahSakit:daftarRS"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "daftar.html")

    def test_add_RS_POST(self):
        response = self.client.post(reverse("RumahSakit:daftarRS"),
                {"nama" : "RSJ Test",
                'alamat': "Grogol",
                'jml_prwt': 1,
                "jml_non": 1,})
        self.assertEquals(response.status_code, 302)
    
    def test_get_detailRS(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1
            )

        response = Client().get(reverse("RumahSakit:detailRS", kwargs={'id': rms.id}))
        self.assertEqual(response.status_code, 200)

    def test_get_listRS(self):
        response = Client().get(reverse("RumahSakit:listRS"))
        self.assertEqual(response.status_code, 200)

    def test_get_detailRS_nolPelamar(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1
            )
        jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = Client().get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        
        self.assertEqual(len(jml_prwt), 0)
        self.assertEqual(len(jml_non), 0)
        self.assertEqual(response.status_code, 200)

    def test_get_detailRS_satuPerawat(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1
            )

        prwt = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia'
            ,rumah_sakit=rms)

        jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = Client().get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        
        self.assertEqual(len(jml_prwt), 1)
        self.assertEqual(len(jml_non), 0)
        self.assertEqual(response.status_code, 200)

    def test_get_detailRS_satuNonPerawat(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1
            )

        non = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia'
            ,rumah_sakit=rms)

        jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = Client().get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        
        self.assertEqual(len(jml_prwt), 0)
        self.assertEqual(len(jml_non), 1)
        self.assertEqual(response.status_code, 200)

    def test_get_detailRS_satuForAll(self):
        rms = RS.objects.create(
                nama = "RSJ Test",
                alamat = "Grogol",
                jml_prwt = 1,
                jml_non = 1
            )

        prwt = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia'
            ,rumah_sakit=rms)

        non = VolunteerNonPerawat.objects.create(nama='Anduk', umur=69, 
            email='Andukin@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593469',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Kasur', alasan='Ingin membantu sesama hewan'
            ,rumah_sakit=rms)

        jml_prwt = Perawat.objects.filter(rumah_sakit_id = rms.id)
        jml_non = VolunteerNonPerawat.objects.filter(rumah_sakit_id = rms.id)
        response = Client().get(reverse("RumahSakit:list-pendaftar", kwargs={'id': rms.id}))
        
        self.assertEqual(len(jml_prwt), 1)
        self.assertEqual(len(jml_non), 1)
        self.assertEqual(response.status_code, 200)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(RumahsakitConfig.name, 'RumahSakit')
        self.assertEqual(apps.get_app_config('RumahSakit').name, 'RumahSakit')