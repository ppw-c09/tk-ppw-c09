from django.shortcuts import render, HttpResponse, redirect
from .forms import FormPelamar
from .models import Jobseeker
from Kantor.models import Kantor
# Create your views here.


choices = [
    ('1', 'Aceh'),
    ('2', 'Sumatra Utara'),
    ('3', 'Sumatra Barat'),
    ('4', 'Riau'),
    ('5', 'Kepulauan Riau'),
    ('6', 'Bengkulu'),
    ('7', 'Jambi'),
    ('8', 'Sumatra Selatan'),
    ('9', 'Bangka Belitung'),
    ('10', 'Lampung'),
    ('11', 'Banten'),
    ('12', 'Jawa Barat'),
    ('13', 'Jakarta'),
    ('14', 'Jawa Tengah'),
    ('15', 'Yogyakarta'),
    ('16', 'Jawa Timur'),
    ('17', 'Bali'),
    ('18', 'NTB'),
    ('19', 'NTT'),
    ('20', 'Kalimantan Timur'),
    ('21', 'Kalimantan Barat'),
    ('22', 'Kalimantan Selatan'),
    ('23', 'Kalimantan Tengah'),
    ('24', 'Kalimantan Utara'),
    ('25', 'Gorontalo'),
    ('26', 'Sulawesi Barat'),
    ('27', 'Sulawesi Tengah'),
    ('28', 'Sulawesi Selatan'),
    ('29', 'Sulawesi Tenggara'),
    ('30', 'Sulawesi Utara'),
    ('31', 'Maluku'),
    ('32', 'Maluku Utara'),
    ('33', 'Papua Barat'),
    ('34', 'Papua'),
]
CHOICES_CLEANED = []
for i in choices:
    CHOICES_CLEANED.append(i[1])


def testFrontEnd(request, job_id):
    form_pelamar = FormPelamar()
    kantor_dilamar = Kantor.objects.get(pk=job_id)
    if request.method == "POST":
        form_pelamar_input = FormPelamar(request.POST)
        if form_pelamar_input.is_valid():
            data = form_pelamar_input.cleaned_data
            pelamarBaru = Jobseeker()
            pelamarBaru.nama_pelamar = data['nama_pelamar']
            pelamarBaru.umur_pelamar = data['umur_pelamar']
            pelamarBaru.provinsi_pelamar = request.POST['provinsi_pelamar']
            pelamarBaru.kota_pelamar = data['kota_pelamar']
            pelamarBaru.pekerjaan_dilamar = kantor_dilamar
            pelamarBaru.pendidikan_terakhir = data['pendidikan_terakhir']
            pelamarBaru.save()
            return redirect('/Kantor')
        else:
            return render(request, 'Jobseeker/addPelamar.html', {'form': form_pelamar, 'status': 'failed', 'listProvinsi': CHOICES_CLEANED})
    else:
        return render(request, 'Jobseeker/addPelamar.html', {'form': form_pelamar, 'listProvinsi': CHOICES_CLEANED, 'nama_pekerjaan': kantor_dilamar.nama_pekerjaan})
