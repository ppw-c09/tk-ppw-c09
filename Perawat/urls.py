from django.urls import path
from Perawat.views import daftar_perawat

urlpatterns = [
    path('daftar-perawat/<int:id>', daftar_perawat, name='daftarperawat'),
]
