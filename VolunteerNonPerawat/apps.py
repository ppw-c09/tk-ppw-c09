from django.apps import AppConfig

class VolunteerNonPerawatConfig(AppConfig):
    name = 'VolunteerNonPerawat'