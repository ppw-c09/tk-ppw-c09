from django.urls import path
from Kantor.views import list_lowongan, detail_pekerjaan, daftar_lowongan, list_applicants

urlpatterns = [
    path('', list_lowongan, name='list-lowongan'),
    path('tambah-lowongan/', daftar_lowongan, name='tambah-lowongan'),
    path('<pk>', detail_pekerjaan, name='detail-lowongan'),
    path('list-of-applicants/P<int:job_id>',
         list_applicants, name='list-of-applicants'),


    #path('<kode>', detail_course, name='detail-course'),
]
