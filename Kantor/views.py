from django.shortcuts import render, redirect
from Kantor.forms import FormKantor
from Kantor.models import Kantor
from Jobseeker.models import Jobseeker
TIPE_PEKERJAAN = [
    ("Fulltime"),
    ("Part time"),
    ("Internship")
]


def list_lowongan(request):
    context = {}
    all_pekerjaan = Kantor.objects.all()

    context['pekerjaans'] = all_pekerjaan

    return render(request, 'Kantor/list-pekerjaan.html', context)


def detail_pekerjaan(request, pk):
    context = {}
    pekerjaan = Kantor.objects.get(pk=pk)

    context['pekerjaan'] = pekerjaan

    return render(request, 'Kantor/detail-pekerjaan.html', context)


def daftar_lowongan(request):
    context = {}
    form = FormKantor()

    if request.method == "POST":
        form_pekerjaan = FormKantor(request.POST)
        if form_pekerjaan.is_valid():
            data = form_pekerjaan.cleaned_data
            pekerjaan = Kantor()
            pekerjaan.nama_pekerjaan = data['nama_pekerjaan']
            pekerjaan.alamat = data['alamat']
            pekerjaan.tipe_pekerjaan = data['tipe_pekerjaan']
            pekerjaan.deskripsi_kantor = data['desc']
            pekerjaan.save()

            context['form'] = form
            context['success'] = True
            context['nama_pekerjaan'] = pekerjaan.nama_pekerjaan

            return redirect('/Kantor')

    return render(request, 'Kantor/daftarkan-pekerjaan.html', {'form': form})


def list_applicants(request, job_id):
    context = {}
    pekerjaanDilamar = Kantor.objects.get(pk=job_id)

    context['pekerjaan'] = pekerjaanDilamar
    context['listPelamar'] = Jobseeker.objects.filter(
        pekerjaan_dilamar_id=job_id)
    # pekerjaan.applicantsss????

    return render(request, 'Kantor/list-applicants.html', context)
