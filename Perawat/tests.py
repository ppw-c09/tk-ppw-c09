from django.test import TestCase, Client
from django.apps import apps
from django.urls import reverse
from Perawat.models import Perawat
from RumahSakit.models import RS
from Perawat.apps import PerawatConfig

class TestPerawat(TestCase):
    def setUp(self):
        rs = RS.objects.create(
            nama='aaaa',
            alamat = 'Jonggol',
            jml_prwt = '2',
            jml_non = '2'
        )
        rs.save()

    def test_url(self):
        response = Client().get(reverse('daftarperawat', args=[1]))
        self.assertEquals(response.status_code, 200)

    def test_template(self):
        response = Client().get(reverse('daftarperawat', args=[1]))
        self.assertTemplateUsed(response, 'daftar-perawat.html')

    def test_obj(self):
        # rs = RS.objects.create(nama ='aaaa', alamat='Jonggol', jml_prwt = '2', jml_non = '2')
        ex = Perawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia')
        self.assertEquals(Perawat.objects.all().count(), 1)

    def test_form(self):
        res = Client().post(reverse('daftarperawat', args=[1]), data={'nama' : 'Budi', 'umur' : '19', 
            'email' : 'budibudiman@gmail.com', 'provinsi' : '13', 'kota' : 'Jakarta Timur', 'no_telp' : '081219593496',
            'pengalaman' : 'Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', 'alasan' : 'Ingin membantu sesama manusia'})
        self.assertEquals(res.status_code, 302)
        self.assertEquals(Perawat.objects.all().count(), 1)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(PerawatConfig.name, 'Perawat')
        self.assertEqual(apps.get_app_config('Perawat').name, 'Perawat')
