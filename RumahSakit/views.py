from django.shortcuts import render, redirect
from .models import RS
from Perawat.models import Perawat
from VolunteerNonPerawat.models import VolunteerNonPerawat
from .forms import RSForm
from django.contrib import messages
from itertools import zip_longest

# Create your views here.

def daftarRS(request):
    if request.method == "POST":
        RS_form = RSForm(request.POST)
        if RS_form.is_valid():
            RS_form.save()
        return redirect("RumahSakit:daftarRS")
    
    context = {
        "RSform" : RSForm
    }

    return render(request, 'daftar.html', context)

def listRS(request):
    context = {}
    all_RS = RS.objects.all()

    context['all_RS'] = all_RS

    return render(request, 'list-RS.html', context)

def detailRS(request, id):
    context = {}
    RSakit = RS.objects.get(id=id)

    context['RS'] = RSakit

    return render(request, 'detail-RS.html', context)

def list_pendaftar(request, id):
    context = {}
    rumahsakit = RS.objects.get(id = id)
    context['RS'] = rumahsakit
    listPerawat = Perawat.objects.filter(rumah_sakit_id = id)
    listNonPerawat = VolunteerNonPerawat.objects.filter(rumah_sakit_id = id)
    context['listVolunteer'] = zip_longest(listPerawat, listNonPerawat)

    return render(request, 'list_pendaftar.html', context)
