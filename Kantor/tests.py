from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps

from Kantor.models import Kantor
from Kantor.forms import FormKantor
from Kantor.models import Kantor
from Kantor.views import daftar_lowongan
from Kantor.apps import KantorConfig
from Jobseeker.models import Jobseeker



# Create your tests here.
class ModelTestKantor(TestCase):
    def setUp(self):
        self.pembuat_lowongan = Kantor.objects.create(
            nama_pekerjaan="Tukang Gali Kubur",
            alamat="Kuburan",
            tipe_pekerjaan="Internship"
        )

    def test_instance_created(self):
        self.assertEqual(Kantor.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.pembuat_lowongan), "Tukang Gali Kubur")


class FormTest(TestCase):
    def test_form_is_valid(self):
        form_kantor = FormKantor(
            data={
                'nama_pekerjaan': "Tukang Gali Kubur",
                'alamat': "Kuburan",
                'tipe_pekerjaan': "Part time",
                'desc':'ngegali kubur'
            }
        )
        self.assertTrue(form_kantor.is_valid())

    def test_form_invalid(self):
        form_kantor = FormKantor(
            data={
            }
        )
        self.assertFalse(form_kantor.is_valid())


class UrlTest(TestCase):
    def test_url_listKantor(self):
        response = Client().get(reverse("list-lowongan"))
        self.assertEqual(response.status_code, 200)


class ViewTest(TestCase):
    def test_templateCheck_tambahLowongan(self):
        response = Client().post(reverse("tambah-lowongan"))
        self.assertTemplateUsed(response, 'Kantor/daftarkan-pekerjaan.html')

    def test_post_tambahLowongan(self):
        response = Client().post(reverse("tambah-lowongan"), 
                {
                    'nama_pekerjaan': "Tukang Gali Kubur",
                    'alamat': "Kuburan",
                    'tipe_pekerjaan': "Part time",
                    'desc':'ngegali kubur'
                }
            )
        self.assertEqual(response.status_code, 302)


    def test_get_detailLowongan(self):
        pekerjaan = Kantor.objects.create(
                nama_pekerjaan="Tukang Gali Kubur",
                alamat= "Kuburan",
                tipe_pekerjaan = "Part time",
                deskripsi_kantor = 'ngegali kubur'
            )

        response = Client().get(reverse("detail-lowongan", kwargs={'pk': pekerjaan.pk}))
        self.assertEqual(response.status_code, 200)

    def test_get_listLowongan_nolPelamar(self):
        pekerjaan = Kantor.objects.create(
                nama_pekerjaan="Tukang Gali Kubur",
                alamat= "Kuburan",
                tipe_pekerjaan = "Part time",
                deskripsi_kantor = 'ngegali kubur'
            )
        jumlah_pelamar = Jobseeker.objects.filter(pekerjaan_dilamar_id=pekerjaan.pk)
        response = Client().get(reverse("list-of-applicants", kwargs={'job_id': pekerjaan.pk}))
        
        self.assertEqual(len(jumlah_pelamar), 0)
        self.assertEqual(response.status_code, 200)

    def test_get_listLowongan_satuPelamar(self):
        pekerjaan = Kantor.objects.create(
                nama_pekerjaan="Tukang Gali Kubur",
                alamat= "Kuburan",
                tipe_pekerjaan = "Part time",
                deskripsi_kantor = 'ngegali kubur'
            )
        pelamar = Jobseeker.objects.create(
            nama_pelamar="Joni",
            umur_pelamar=17,
            provinsi_pelamar="1",
            kota_pelamar="Bogor",
            pendidikan_terakhir="MA",
            pekerjaan_dilamar = pekerjaan
        )

        jumlah_pelamar = Jobseeker.objects.filter(pekerjaan_dilamar_id=pekerjaan.pk)
        response = Client().get(reverse("list-of-applicants", kwargs={'job_id': pekerjaan.pk}))
        
        self.assertEqual(len(jumlah_pelamar), 1)
        self.assertEqual(response.status_code, 200)


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(KantorConfig.name, 'Kantor')
        self.assertEqual(apps.get_app_config('Kantor').name, 'Kantor')