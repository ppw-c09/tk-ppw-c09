from django.urls import path

from . import views

app_name = 'TK1_PPW'

urlpatterns = [
    path('', views.home, name='home')
]