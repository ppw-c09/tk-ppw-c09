from django.db import models

TIPE_PEKERJAAN = (
    ("Fulltime", "Fulltime"),
    ("Part time", "Part time"),
    ("Internship", "Internship")
)


class Kantor(models.Model):
    nama_pekerjaan = models.CharField(max_length=100)
    alamat = models.CharField(max_length=100)
    tipe_pekerjaan = models.CharField(choices=TIPE_PEKERJAAN, max_length=15)
    tersedia_sejak = models.DateField(auto_now=True, auto_created=True)
    deskripsi_kantor = models.CharField(max_length=10000)

    def __str__(self):
        return self.nama_pekerjaan
