from django import forms
from .models import RS

class RSForm(forms.ModelForm):
    nama = forms.CharField(
        label="Nama Rumah Sakit",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

    alamat = forms.CharField(
        label="Alamat",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

    class Meta:
        model = RS
        fields = "__all__"