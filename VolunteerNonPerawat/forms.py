from django.forms import ModelForm
from .models import VolunteerNonPerawat

class AddForm(ModelForm):
    class Meta:
        model = VolunteerNonPerawat
        fields = [
            'nama',
            'umur',
            'email',
            'provinsi',
            'kota',
            'no_telp',
            'pengalaman',
            'alasan'
        ]