from django.test import TestCase, Client
from django.apps import apps
from django.urls import reverse
from .models import VolunteerNonPerawat, RS
from .apps import VolunteerNonPerawatConfig

class Test(TestCase):
    def setUp(self):
        rs = RS.objects.create(nama ='aaaa', alamat='Jonggol', jml_prwt = '2', jml_non = '2')
        rs.save()
    def test_00_urls(self):
        response = Client().get(reverse('VolunteerNonPerawat:index', args=[1]))
        self.assertEqual(response.status_code, 200)
    def test_01_template_used(self):
        response = Client().get(reverse('VolunteerNonPerawat:index', args=[1]))
        self.assertTemplateUsed(response, 'index.html')
    def test_02_create_object(self):
        cnt = VolunteerNonPerawat.objects.all().count()
        vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
            email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
            pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia')
        self.assertEquals(VolunteerNonPerawat.objects.all().count(), cnt + 1)
    def test_03_create_form(self):
        cnt = VolunteerNonPerawat.objects.all().count()
        response = Client().post(reverse('VolunteerNonPerawat:add-vnp', args=[1]), {'nama' : 'Budi', 'umur' : '19', 
            'email' : 'budibudiman@gmail.com', 'provinsi' : '13', 'kota' : 'Jakarta Timur', 'no_telp' : '081219593496',
            'pengalaman' : 'Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', 'alasan' : 'Ingin membantu sesama manusia'})
        self.assertEquals(response.status_code, 302)
        self.assertEquals(VolunteerNonPerawat.objects.all().count(), cnt + 2)
    # def test_04_delete_form(self):
    #     vnp = VolunteerNonPerawat.objects.create(nama='Budi', umur=19, 
    #         email='budibudiman@gmail.com', provinsi='13', kota='Jakarta Timur', no_telp='081219593496',
    #         pengalaman='Menjadi relawan kemanusiaan saat terjadi gempa di Lombok', alasan='Ingin membantu sesama manusia')
    #     cnt = VolunteerNonPerawat.objects.all().count()
    #     response = Client().post(f'/VolunteerNonPerawat/delete/{VolunteerNonPerawat.objects.get(nama="Budi").id}/')
    #     self.assertEquals(response.status_code, 302)
    #     self.assertEquals(VolunteerNonPerawat.objects.all().count(), cnt - 1)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(VolunteerNonPerawatConfig.name, 'VolunteerNonPerawat')
        self.assertEqual(apps.get_app_config('VolunteerNonPerawat').name, 'VolunteerNonPerawat')
