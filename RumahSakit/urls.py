from django.urls import path
from . import views

app_name = 'RumahSakit'

urlpatterns = [
    path('daftarRS/', views.daftarRS, name='daftarRS'),
    path('listRS/', views.listRS, name = 'listRS'),
    path('<int:id>/', views.detailRS, name='detailRS'),
    path('ListPendaftar/<int:id>', views.list_pendaftar, name='list-pendaftar')
]