from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponseRedirect
from .forms import AddForm
from .models import VolunteerNonPerawat
from RumahSakit.models import RS

choices = [
    ('1', 'Aceh'),
    ('2', 'Sumatra Utara'),
    ('3', 'Sumatra Barat'),
    ('4', 'Riau'),
    ('5', 'Kepulauan Riau'),
    ('6', 'Bengkulu'),
    ('7', 'Jambi'),
    ('8', 'Sumatra Selatan'),
    ('9', 'Bangka Belitung'),
    ('10', 'Lampung'),
    ('11', 'Banten'),
    ('12', 'Jawa Barat'),
    ('13', 'DKI Jakarta'),
    ('14', 'Jawa Tengah'),
    ('15', 'Yogyakarta'),
    ('16', 'Jawa Timur'),
    ('17', 'Bali'),
    ('18', 'NTB'),
    ('19', 'NTT'),
    ('20', 'Kalimantan Timur'),
    ('21', 'Kalimantan Barat'),
    ('22', 'Kalimantan Selatan'),
    ('23', 'Kalimantan Tengah'),
    ('24', 'Kalimantan Utara'),
    ('25', 'Gorontalo'),
    ('26', 'Sulawesi Barat'),
    ('27', 'Sulawesi Tengah'),
    ('28', 'Sulawesi Selatan'),
    ('29', 'Sulawesi Tenggara'),
    ('30', 'Sulawesi Utara'),
    ('31', 'Maluku'),
    ('32', 'Maluku Utara'),
    ('33', 'Papua Barat'),
    ('34', 'Papua'),
]
CHOICES_CLEANED = []
for i in choices:
    CHOICES_CLEANED.append(i[1])

def index(request, id):
    form_pelamar = AddForm()
    vnp_list = VolunteerNonPerawat.objects.all()
    return render(request, 'index.html', {'form' : form_pelamar, 'listProvinsi': CHOICES_CLEANED, 'vnp_list' : vnp_list, 'id' : id})

def add_vnp(request, id):
    form = AddForm(request.POST or None)
    rumahsakit = RS.objects.get(id=id)
    if (form.is_valid() and request.method == 'POST'):
        data = form.cleaned_data
        vnp = VolunteerNonPerawat()
        vnp.nama = data['nama']
        vnp.umur = data['umur']
        vnp.email = data['email']
        vnp.provinsi = request.POST['provinsi']
        vnp.kota = data['kota']
        vnp.pengalaman = data['pengalaman']
        vnp.alasan = data['alasan']
        vnp.rumah_sakit = rumahsakit
        vnp.save()
        form.save()
    return HttpResponseRedirect('/')

# def delete_vnp(request, id):
#     activity = get_object_or_404(VolunteerNonPerawat, id=id)
#     activity.delete()
#     return redirect('VolunteerNonPerawat:index')
