from django.shortcuts import render, redirect
from Perawat.forms import FormPerawat
from RumahSakit.models import RS
from Perawat.models import Perawat

def daftar_perawat(request, id):
    form  = FormPerawat()
    rumahsakit = RS.objects.get(id = id)
    if request.method == 'POST':
        formbaru = FormPerawat(request.POST)
        if formbaru.is_valid():
            data = formbaru.cleaned_data
            perawat = Perawat()
            perawat.nama = data['nama']
            perawat.umur = data['umur']
            perawat.email = data['email']
            perawat.provinsi = data['provinsi']
            perawat.kota = data['kota']
            perawat.no_telp = data['no_telp']
            perawat.pengalaman = data['pengalaman']
            perawat.alasan = data['alasan']
            perawat.rumah_sakit = rumahsakit
            perawat.save()
            return redirect('home')
    context = {
        'form' : form,
        'page_title' : 'Daftar Menjadi Relawan Sekarang'
    }
    return render(request, 'daftar-perawat.html', context)